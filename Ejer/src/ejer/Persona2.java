/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejer;

/**
 *
 * @author reroes
 */
public class Persona2 {
    private String nombre;
    private String apellido;
    private int edad;
    private int hijos;
    
    public Persona2(String x){
        this.nombre = x;
        
    }
    
    public Persona2(String x, String y){
        this.nombre = x;
        this.apellido = y;
        
    }
    
    public Persona2(String x, String y, int z){
        this.nombre = x;
        this.apellido = y;
        this.edad = z;
        
    }
       public Persona2(String x, String y, int z,int q){
        this.nombre = x;
        this.apellido = y;
        this.edad = z;
        this.hijos = q;
       }
    public void establecesNombre(String x){
        nombre = x;
    }
    public void establecesApellido(String y){
        apellido = y;
    }
    public void establecerEdad(int z){
        edad = z;
    }
    
    public String obtenerNombre(){
        return nombre;
    }
    
    public String obtenerApellido(){
        return apellido;
    }
    
    public int obtenerEdad(){
        return edad;
    }

    public int getHijos() {
        return hijos;
    }

    public void setHijos(int hijos) {
        this.hijos = hijos;
    }
    
    
    public String cadenaPersona(){
        String cadena = String.format("Pesona llamada %s %s, tiene una edad de %d : y tiene %d hijos", obtenerNombre(), obtenerApellido(), obtenerEdad(), getHijos());
        return cadena;
    }
}